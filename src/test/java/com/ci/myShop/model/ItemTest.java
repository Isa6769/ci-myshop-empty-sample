package com.ci.myShop.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ItemTest {
	
	@Test
	public void createItemTest() {
		Item item = new Item("name", 0, 100.2f, 10);
		assertEquals(item.getName(), "name");
		}
	
	
	@Test
	public void createEmptyItem() {
		Item item = new Item();
		assertNotEquals(null, item);
	}
	
	
	protected Item itemTest;
	
	@Test
		public void displayItemTest() {
		itemTest = new Item("name", 0, 100, 10);
		assertEquals(itemTest.display(), "Nom : name, Id : 0, Prix : 100.0, Nb : 10");
		}

}
