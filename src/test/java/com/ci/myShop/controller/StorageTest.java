package com.ci.myShop.controller;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.ci.myShop.controller.Storage;
import com.ci.myShop.model.Item;

public class StorageTest {

	Map<String, Item> itemMap = new HashMap();


//	@Test
//	public void addItem() {
//		Storage storage = new Storage(itemMap);
//		
//		storage.addItem(item);
//		assertEquals(1, storage.getItem().values().size());

//	}

	@Test
	public void getItem() {
		Storage storageTest = new Storage(itemMap);
		Item it1 = new Item("test", 2, 100f, 1000);
		storageTest.addItem(it1);
		Item extractedItem = storageTest.getItem("test");
		assertEquals(extractedItem, it1);
	}
}
