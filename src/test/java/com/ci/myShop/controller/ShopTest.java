package com.ci.myShop.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Test;

import com.ci.myShop.model.Item;

public class ShopTest {
	
	protected Map storageTest;
	
	@Test
	public void createShopTest() {
		Map<String, Item> itemMap=new HashedMap();
		Storage storage=new Storage(itemMap);
		Shop shop = new Shop(storage);
		assertNotEquals(null, shop);
	}
	
	@Test
	// Test de la fonction sell si l'Item appartient au storage du shop
	public void sellTest() {
		Map<String, Item> itemMap=new HashedMap();
		Storage storage=new Storage(itemMap);
		Shop shop = new Shop(storage);
		Item it1 = new Item("it1", 2, 5.0f, 10);
		storage.addItem(it1);
		shop.sell("it1", 4);
		assertEquals(20, shop.getCash(),0);
	}
	
	@Test
	// Test de la fonction sell si l'Item n'appartient pas au storage du shop
	public void sellTestBis() {
		Map<String, Item> itemMap=new HashedMap();
		Storage storage=new Storage(itemMap);
		Shop shop = new Shop(storage);
		float cash = 100;
		shop.setCash(cash);
		Item it4 = new Item("it4", 2, 6.0f, 10);
		shop.sell("it4", 6);
		assertEquals(100, shop.getCash(),0);
	}
	
	@Test
	// Test de la fonction buy si j'ai assez de cash
	public void buyTest() {
		Map<String, Item> itemMap=new HashedMap();
		Storage storage=new Storage(itemMap);
		Shop shop = new Shop(storage);
		float cash = 100;
		shop.setCash(cash);
		Item it1=new Item("it1", 2, 4.0f, 10);
		storage.addItem(it1);
		shop.buy(it1, 3);
		assertEquals(88, shop.getCash(),0);
	}
	
	@Test
	// Test de la fonction buy si le montant du cash n'est pas suffisant
	public void buyTestBis() {
		Map<String, Item> itemMap=new HashedMap();
		Storage storage=new Storage(itemMap);
		Shop shop = new Shop(storage);
		float cash = 10;
		shop.setCash(cash);
		Item it2=new Item("it2", 3, 3.0f, 100);
		storage.addItem(it2);
		shop.buy(it2, 10);
		assertEquals(10, shop.getCash(),0);
	}
	
}
