package com.shop.starter;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.ci.myShop.controller.Shop;
import com.ci.myShop.controller.Storage;
import com.ci.myShop.model.Item;

public class Launcher {
	public static void main(String[] args) {
			
		// cree une liste d'Item
		Map<String, Item> itemMap = new HashMap();
		
		// cree deux Items (pour la fonction sell)
		Item article1 = new Item("Article1", 1, 10, 100);
		Item article2 = new Item("Article2", 2, 20, 200);

		// cree deux Items (pour la fonction buy)
		Item article3 = new Item("Article3", 3, 15, 150);
		Item article4 = new Item("Article4", 4, 25, 100);

		// associe les 2 Items a la liste creee (pour la fonction sell)
		itemMap.put(article1.getName(), article1);
		itemMap.put(article2.getName(), article2);

		// associe les 2 Items a la liste creee (pour la fonction buy)
		itemMap.put(article3.getName(), article3);

		// cree un Shop initialise avec la liste
		Storage storage = new Storage(itemMap);
		Shop shop = new Shop(storage);
		float cash = 1000;
		
		shop.setCash(cash);

		// execute la fonction sell du Shop avec le nom d'un item existant
		// on ne peut vendre un article que si on l’a dans le Shop

		// selectionne un item a acheter et une quatite a acheter
		String nomChoix = "Article2";
		int qteChoix = 30;
		Item item = storage.getItem(nomChoix);
		
		if (item == null) {
			System.out.println("Article non reference en magasin");
		} else if (qteChoix <= item.getNbrElt()) {
			System.out.println("Cash du magasin avant la vente = " + cash);
			shop.sell(nomChoix, qteChoix);
			System.out.println("\nResume de la vente \nNom article = " + item.getName() + " - Quantitee vendue = "
					+ qteChoix + " - Prix unitaire = " + item.getPrice());

			System.out.println("Cash du magasin apres la vente = " + shop.getCash());
			System.out.println("Stock actualise apres la vente = " + (item.getNbrElt() - qteChoix)
					+ " (Stock initial = " + item.getNbrElt() + ")");
		} else {
			System.out.println("Article reference mais stock insuffisant");
			System.out.println(" Nom de l’article  = "  +  item.getName() + " - Quantite souhaitee = " + qteChoix + " - Quantite max en stock = " + item.getNbrElt());}

	

		// execute la fonction buy du Shop avec l'item non present dans la liste
		// on peut acheter un article meme si l’article n’existe pas dans le Shop (mais
		// il a du etre cree en amont).
		
		// ATTN : Si achat d'un article du storage, modifier la quantite qteAchat.
		// ATTN : si achat d'un article pas du storage, modifier la quantite dans NbrElt article2.
		Item articleAchat = new Item("", 0, 0, 0);
		articleAchat = article3;
		int qteAchat = 2000;

		if (itemMap.containsValue(articleAchat) == true && qteAchat * articleAchat.getPrice() <= cash) {
			System.out.println("L'article achete est reference dans le magasin.");
			System.out.println("Cash du magasin avant l'achat = " + shop.getCash());
			shop.buy(articleAchat, qteAchat);
			System.out.println("\nResume de l'achat \nNom article = " + articleAchat.getName()
					+ " - Quantite achetee = " + qteAchat + " - Prix unitaire = " + articleAchat.getPrice());
			System.out.println("Cash du magasin apres l'achat = " + shop.getCash());
			System.out.println("Stock actualise apres l'achat = " + (articleAchat.getNbrElt() + qteAchat)
					+ " (Stock initial = " + articleAchat.getNbrElt() + ")");

		} else if (itemMap.containsValue(articleAchat) == false && articleAchat.getNbrElt() * articleAchat.getPrice() <= cash) {
			System.out.println("L'article n'est pas reference dans le magasin.");
			System.out.println("Cash du magasin avant l'achat = " + shop.getCash());
			shop.buy(articleAchat, articleAchat.getNbrElt());
			System.out.println(
					"\nResume de l'achat \nNom nouvel article = " + articleAchat.getName() + " - Quantite achetee = "
							+ articleAchat.getNbrElt() + " - Prix unitaire = " + articleAchat.getPrice());
			System.out.println("Cash du magasin apres l'achat = " + shop.getCash());
			System.out.println(
					"Stock actualise apres l'achat = " + articleAchat.getNbrElt() + " (Pas en stock avant l'achat)");

		} else {
			System.out.println("Pas assez de cash pour l'achat \nReference ajoutee ou non dans le storage");
		}

	}
}

