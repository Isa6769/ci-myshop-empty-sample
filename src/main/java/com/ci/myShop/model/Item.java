package com.ci.myShop.model;

public class Item {
	private String name;
	private int id;
	private float price;
	private int nbrElt;

	public Item(String name, int id, float price, int nbrElt) {
		super();
		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
	}
	
	// methode pour creer un Item vide (utile pour faire les tests sur l'Item)
	public Item() {
		
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public float getPrice() {
		return price;
	}

	public int getNbrElt() {
		return nbrElt;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}

	public String display() {
		String result = "Nom : " + name + ", Id : " + id + ", Prix : " + price + ", Nb : " + nbrElt;
		return result;
	}
	
}
