package com.ci.myShop.controller;

import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Templates;

import org.apache.commons.collections.map.HashedMap;

import com.ci.myShop.model.Item;

public class Storage {

	private Map<String, Item> itemMap = new HashMap();

	public Storage(Map<String, Item> itemMap) {
		super();
		this.itemMap = itemMap;
	}
	

	// ajoute un item a l'itemMap
	public void addItem(Item obj) {
		itemMap.put(obj.getName(), obj);
	}

	// recupere un objet de l'itemMap
	public Item getItem(String name) {
		return itemMap.get(name);
	}
}
