package com.ci.myShop.controller;

import java.util.Map;

import org.apache.commons.collections.map.HashedMap;

import com.ci.myShop.model.Item;

public class Shop {

	private Storage storage;
	private float cash;
	
	public Shop(Storage storage) {
		super();
		this.storage = storage;
	}

	// permet de vendre un item s'il existe	
	int qte;
	public Item sell(String name, int qte) {
		Item item = storage.getItem(name);
		if (item != null) {
			cash = cash + qte*item.getPrice();
				}
		return item;
	}

	// fonction booleenne permet au Shop d'acheter l'item s'il a suffisamment de cash (question 4.3 – binome 2)
	public boolean buy(Item item, int qte) {
		if (item.getClass()!= null && qte*item.getPrice() <= cash) {
			storage.addItem(item);
			cash = cash - qte*item.getPrice();
			return true;
		}
		return false;
	}	
	
	// recupere le cash du shop (question 4.3 - binome 2)
		public float getCash() {
			return cash;
		}

		public void setCash(float cash) {
			this.cash = cash;
		}
}